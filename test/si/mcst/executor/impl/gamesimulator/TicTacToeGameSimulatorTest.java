package si.mcst.executor.impl.gamesimulator;

import junit.framework.Assert;
import org.junit.Test;
import si.mcst.executor.gamesimulator.GameActionsAbstract;
import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.tictactoe.impl.TicTacToeGameActions;
import si.tictactoe.impl.TicTacToeGameSimulator;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 18:00
 */
public class TicTacToeGameSimulatorTest {
    GameActionsAbstract gameActions = new TicTacToeGameActions();
    int[][] playingBoard;
    GameSimulatorAbstract gameSimulator;

    @Test
    public void testGetCurrentlyAvailableActions() throws Exception {
        int firstPlayer =1;

        playingBoard = new int[][]{ {0, 0, 0},
                                    {0, 0, 0},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);

        gameSimulator.simulateAction(4);

        Assert.assertFalse(gameSimulator.GetCurrentlyAvailableActions().contains(4));
        Assert.assertFalse(gameSimulator.isSimulationInFinalState());
        System.out.println(gameSimulator.getSimulationResult());

        gameSimulator.simulateAction(0);

        Assert.assertFalse(gameSimulator.GetCurrentlyAvailableActions().contains(0));
        Assert.assertFalse(gameSimulator.isSimulationInFinalState());
        System.out.println(gameSimulator.getSimulationResult());

        gameSimulator.simulateAction(6);

        Assert.assertFalse(gameSimulator.GetCurrentlyAvailableActions().contains(6));
        Assert.assertFalse(gameSimulator.isSimulationInFinalState());
        System.out.println(gameSimulator.getSimulationResult());

        gameSimulator.simulateAction(2);

        Assert.assertFalse(gameSimulator.GetCurrentlyAvailableActions().contains(2));
        Assert.assertFalse(gameSimulator.isSimulationInFinalState());
        System.out.println(gameSimulator.getSimulationResult());

        gameSimulator.simulateAction(3);

        Assert.assertFalse(gameSimulator.GetCurrentlyAvailableActions().contains(3));
        Assert.assertFalse(gameSimulator.isSimulationInFinalState());
        System.out.println(gameSimulator.getSimulationResult());

        gameSimulator.simulateAction(1);

        Assert.assertFalse(gameSimulator.GetCurrentlyAvailableActions().contains(1));
        Assert.assertTrue(gameSimulator.isSimulationInFinalState());
        System.out.println(gameSimulator.getSimulationResult());


    }

    @Test
    public void testSimulateAction() throws Exception {
        int firstPlayer =1;
        playingBoard = new int[][]{ {0, 0, 0},
                                    {0, 0, 0},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        gameSimulator.simulateAction(5);
        gameSimulator.simulateAction(3);
        gameSimulator.simulateAction(1);

        Assert.assertEquals(gameSimulator.GetCurrentlyAvailableActions().size(), gameActions.GetAllActions().size() - 3);
        Assert.assertFalse(gameSimulator.GetCurrentlyAvailableActions().contains(5));

        playingBoard = new int[][]{ {0, 0, 0},
                                    {0, 0, 0},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
    }

    @Test
    public void testResetSimulation() throws Exception {
        int firstPlayer =1;
        playingBoard = new int[][]{ {0, 0, 0},
                                    {0, 0, 0},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        gameSimulator.simulateAction(5);
        gameSimulator.resetSimulation();

        Assert.assertTrue(gameSimulator.GetCurrentlyAvailableActions().contains(5));
    }

    @Test
    public void testIsSimulationInFinalState() throws Exception {
        int firstPlayer =1;
        //One move left
        playingBoard = new int[][]{ {1, 2, 1},
                                    {1, 2, 1},
                                    {2, 1, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertFalse(gameSimulator.isSimulationInFinalState());

        //No moves left
        playingBoard = new int[][]{ {1, 2, 1},
                                    {1, 2, 1},
                                    {2, 1, 2}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertTrue(gameSimulator.isSimulationInFinalState());

        //three in a row
        playingBoard = new int[][]{ {1, 1, 1},
                                    {0, 0, 0},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertTrue(gameSimulator.isSimulationInFinalState());

    }

    @Test
    public void testGetSimulationResult() throws Exception {
        int firstPlayer =1;

        //1 wins, reward 1
        playingBoard = new int[][]{ {1, 1, 1},
                                    {0, 0, 0},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(1d,gameSimulator.getSimulationResult());

        //No one wins, reward 0.5
        playingBoard = new int[][]{ {2, 1, 1},
                                    {0, 0, 0},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(0.5d,gameSimulator.getSimulationResult());

        //No one wins, reward 0.5
        playingBoard = new int[][]{ {1, 2, 1},
                                    {2, 2, 1},
                                    {2, 1, 2}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(0.5d,gameSimulator.getSimulationResult());

        //2 wins, reward 0
        playingBoard = new int[][]{ {1, 1, 2},
                                    {2, 2, 2},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(0d,gameSimulator.getSimulationResult());

        //1 wins, reward 1
        playingBoard = new int[][]{ {0, 0, 0},
                                    {1, 1, 1},
                                    {0, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(1d,gameSimulator.getSimulationResult());

        //1 wins, reward 1
        playingBoard = new int[][]{ {0, 0, 0},
                                    {0, 0, 0},
                                    {1, 1, 1}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(1d,gameSimulator.getSimulationResult());

        //1 wins, reward 1
        playingBoard = new int[][]{ {0, 0, 1},
                                    {0, 1, 0},
                                    {1, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(1d,gameSimulator.getSimulationResult());

        //1 wins, reward 1
        playingBoard = new int[][]{ {1, 0, 0},
                                    {0, 1, 0},
                                    {0, 0, 1}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(1d,gameSimulator.getSimulationResult());

        //1 wins, reward 1
        playingBoard = new int[][]{ {1, 0, 0},
                                    {1, 0, 0},
                                    {1, 0, 0}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(1d,gameSimulator.getSimulationResult());

        //1 wins, reward 1
        playingBoard = new int[][]{ {0, 0, 1},
                                    {0, 0, 1},
                                    {0, 0, 1}};
        gameSimulator = new TicTacToeGameSimulator(playingBoard,gameActions,1, firstPlayer);
        Assert.assertEquals(1d,gameSimulator.getSimulationResult());


    }
}
