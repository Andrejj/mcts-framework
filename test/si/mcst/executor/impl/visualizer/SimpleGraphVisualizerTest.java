package si.mcst.executor.impl.visualizer;


import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.tictactoe.impl.TicTacToeGameActions;
import si.tictactoe.impl.TicTacToeGameSimulator;
import si.mcst.executor.visualizer.VisualizerAbstract;
import si.mcst.executor.visualizer.impl.SimpleGraphVisualizer;
import si.mcst.tree.MCTSearch;
import si.mcst.tree.node.NodeAbstract;
import si.mcst.tree.node.impl.SimpleNode;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 19.3.2013
 * Time: 17:43
 */
public class SimpleGraphVisualizerTest {


    public static void main(String [] args)
    {

        GameSimulatorAbstract gca = new TicTacToeGameSimulator(new int[3][3],new TicTacToeGameActions(),1,1);

        MCTSearch mctSearch = new MCTSearch(gca);
        mctSearch.setRootNode(new SimpleNode());

        NodeAbstract node = mctSearch.getRoot();

        NodeAbstract child1 = node.addChild(0,new SimpleNode(node,-1));
        NodeAbstract child2 = node.addChild(1,new SimpleNode(node,-1));
        NodeAbstract child3 = node.addChild(2,new SimpleNode(node,-1));

        child1.addChild(3,new SimpleNode(child1,-1));
        child1.addChild(4,new SimpleNode(child1,-1));
        child1.addChild(5,new SimpleNode(child1,-1));

        child2.addChild(6,new SimpleNode(child2,-1));
        child2.addChild(7,new SimpleNode(child2,-1));

        child3.addChild(8,new SimpleNode(child3,-1));

        VisualizerAbstract sgv = new SimpleGraphVisualizer();

        sgv.setTree(mctSearch);
        sgv.refresh();
        sgv.refresh();
    }
}
