package si.mcst.executor.impl.gameconnector;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.tictactoe.impl.TicTacToeGameActions;
import si.tictactoe.impl.TicTacToeGameSimulator;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 19:23
 */
public class TicTacToeGameConnectorTest {
    GameSimulatorAbstract connector;
    int[][] gameBoard;

    @Before
    public void setUp() throws Exception {
        gameBoard = new int[3][3];
        connector = new TicTacToeGameSimulator(gameBoard, new TicTacToeGameActions(),1,1);
    }

    @Test
    public void testGetCurrentlyAvailableActions() throws Exception {
        int numberOfAvailableMoves = connector.GetCurrentlyAvailableActions().size();
        Assert.assertEquals(numberOfAvailableMoves,9);
        connector.simulateAction(1);
        List<Integer> moves = connector.GetCurrentlyAvailableActions();
        boolean contains = moves.contains(1);

        Assert.assertFalse(contains);
        Assert.assertEquals(moves.size(),8);
    }

    @Test
    public void testExecuteAction() throws Exception {
        connector.simulateAction(0);
        int numberOfAvailableMoves = connector.GetCurrentlyAvailableActions().size();
        Assert.assertEquals(numberOfAvailableMoves,8);
        Assert.assertFalse(connector.GetCurrentlyAvailableActions().contains(0));
        connector.simulateAction(8);
        Assert.assertFalse(connector.GetCurrentlyAvailableActions().contains(8));
    }
}
