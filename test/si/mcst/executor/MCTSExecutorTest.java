package si.mcst.executor;

import junit.framework.Assert;
import org.junit.Test;
import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.executor.strategy.backpropagation.impl.AverageRewardBackPropagation;
import si.mcst.executor.strategy.expansion.imp.RandomExpansion;
import si.mcst.executor.strategy.selection.impl.RandomSelection;
import si.mcst.executor.strategy.simulation.impl.RandomSimulation;
import si.mcst.executor.visualizer.impl.SimpleGraphVisualizer;
import si.mcst.tree.MCTSearch;
import si.mcst.tree.node.NodeAbstract;
import si.mcst.tree.node.impl.SimpleNode;
import si.tictactoe.impl.TicTacToeGameActions;
import si.tictactoe.impl.TicTacToeGameSimulator;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 25.3.2013
 * Time: 15:15
 */
public class MCTSExecutorTest {
    @Test
    public void testExecuteMCSTSimulation() throws Exception {
        //Java7 issue fix (http://forum.jgraph.com/questions/3002/hierarchical-layout-throws-exception-with-large-graphs)
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");


        int[][] gameBoard = null;
        int firstPlayer =2;

        gameBoard = new int[][]{    {2, 2, 1},
                                    {2, 2, 1},
                                    {0, 1, 0}};
        GameSimulatorAbstract gameConnector =  new TicTacToeGameSimulator(gameBoard,new TicTacToeGameActions(),1, firstPlayer);

        MCTSearch mctSearch = new MCTSearch(gameConnector);
        mctSearch.setRootNode(new SimpleNode());

        MCTSExecutor mctsExecutor = new MCTSExecutor(mctSearch);
        mctsExecutor.setSelectionStrategy(new RandomSelection());
        mctsExecutor.setExpansionStrategy(new RandomExpansion());
        mctsExecutor.setSimulationStrategy(new RandomSimulation());
        mctsExecutor.setBackPropagationStrategy(new AverageRewardBackPropagation());
        //mctsExecutor.addGraphVisualizer(new SimpleGraphVisualizer());

        NodeAbstract resultAction = mctsExecutor.ExecuteMCSTSimulation(100000,0);
        Assert.assertEquals(8,resultAction.getActionFromParent());



        firstPlayer = 1;
        gameBoard = new int[][]{    {2, 2, 1},
                                    {0, 2, 1},
                                    {0, 1, 0}};
        gameConnector =  new TicTacToeGameSimulator(gameBoard,new TicTacToeGameActions(),1, firstPlayer);

        mctSearch = new MCTSearch(gameConnector);
        mctSearch.setRootNode(new SimpleNode());

        mctsExecutor = new MCTSExecutor(mctSearch);
        mctsExecutor.setSelectionStrategy(new RandomSelection());
        mctsExecutor.setExpansionStrategy(new RandomExpansion());
        mctsExecutor.setSimulationStrategy(new RandomSimulation());
        mctsExecutor.setBackPropagationStrategy(new AverageRewardBackPropagation());
        //mctsExecutor.addGraphVisualizer(new SimpleGraphVisualizer());

        resultAction = mctsExecutor.ExecuteMCSTSimulation(100000,0);
        Assert.assertEquals(8,resultAction.getActionFromParent());


        firstPlayer = 2;
        gameBoard = new int[][]{    {2, 1, 1},
                                    {0, 2, 0},
                                    {0, 0, 0}};
        gameConnector =  new TicTacToeGameSimulator(gameBoard,new TicTacToeGameActions(),2, firstPlayer);

        mctSearch = new MCTSearch(gameConnector);
        mctSearch.setRootNode(new SimpleNode());

        mctsExecutor = new MCTSExecutor(mctSearch);
        mctsExecutor.setSelectionStrategy(new RandomSelection());
        mctsExecutor.setExpansionStrategy(new RandomExpansion());
        mctsExecutor.setSimulationStrategy(new RandomSimulation());
        mctsExecutor.setBackPropagationStrategy(new AverageRewardBackPropagation());
        //mctsExecutor.addGraphVisualizer(new SimpleGraphVisualizer());

        resultAction = mctsExecutor.ExecuteMCSTSimulation(100000,0);
        Assert.assertEquals(8,resultAction.getActionFromParent());

        firstPlayer = 1;
        gameBoard = new int[][]{    {2, 1, 1},
                                    {0, 2, 0},
                                    {0, 0, 1}};
        gameConnector =  new TicTacToeGameSimulator(gameBoard,new TicTacToeGameActions(),2, firstPlayer);

        mctSearch = new MCTSearch(gameConnector);
        mctSearch.setRootNode(new SimpleNode());

        mctsExecutor = new MCTSExecutor(mctSearch);
        mctsExecutor.setSelectionStrategy(new RandomSelection());
        mctsExecutor.setExpansionStrategy(new RandomExpansion());
        mctsExecutor.setSimulationStrategy(new RandomSimulation());
        mctsExecutor.setBackPropagationStrategy(new AverageRewardBackPropagation());
        //mctsExecutor.addGraphVisualizer(new SimpleGraphVisualizer());

        resultAction = mctsExecutor.ExecuteMCSTSimulation(100000,0);
        Assert.assertEquals(7,resultAction.getActionFromParent());




        firstPlayer = 1;
        gameBoard = new int[][]{    {2, 1, 1},
                                    {0, 2, 0},
                                    {0, 0, 1}};
        gameConnector =  new TicTacToeGameSimulator(gameBoard,new TicTacToeGameActions(),1, firstPlayer);

        mctSearch = new MCTSearch(gameConnector);
        mctSearch.setRootNode(new SimpleNode());

        mctsExecutor = new MCTSExecutor(mctSearch);
        mctsExecutor.setSelectionStrategy(new RandomSelection());
        mctsExecutor.setExpansionStrategy(new RandomExpansion());
        mctsExecutor.setSimulationStrategy(new RandomSimulation());
        mctsExecutor.setBackPropagationStrategy(new AverageRewardBackPropagation());
        //mctsExecutor.addGraphVisualizer(new SimpleGraphVisualizer());

        resultAction = mctsExecutor.ExecuteMCSTSimulation(100000,0);
        Assert.assertEquals(5,resultAction.getActionFromParent());

    }
}
