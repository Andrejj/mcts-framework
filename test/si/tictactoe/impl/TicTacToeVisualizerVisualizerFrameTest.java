package si.tictactoe.impl;


import si.tictactoe.impl.TicTacToeVisualizerVisualizerFrame;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 20:07
 */
public class TicTacToeVisualizerVisualizerFrameTest {
    public static void main(String [] args)
    {
        int[][] playingBoard = new int[][]{ {2, 1, 2},
                                            {2, 2, 2},
                                            {2, 1, 2}};
        TicTacToeVisualizerVisualizerFrame tttv = new TicTacToeVisualizerVisualizerFrame();
        tttv.refresh(playingBoard);
    }


}
