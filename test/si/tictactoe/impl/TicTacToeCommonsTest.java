package si.tictactoe.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.lang.Thread.*;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 25.3.2013
 * Time: 19:52
 */
public class TicTacToeCommonsTest {
    int[][] gameBoard;

    @Test
    public void testGetCurrentlyAvailableActions() throws Exception {
        gameBoard = new int[][]{    {2, 1, 1},
                                    {0, 2, 0},
                                    {0, 0, 1}};
        List<Integer> availableActions = TicTacToeCommons.GetCurrentlyAvailableActions(gameBoard);

    }

    @Test
    public void testExecuteAction() throws Exception {
        gameBoard = new int[][]{    {2, 1, 1},
                                    {0, 2, 0},
                                    {0, 0, 1}};
        TicTacToeVisualizer visualizer = new TicTacToeVisualizer();
        visualizer.refresh(gameBoard);
        sleep(3000);
        TicTacToeCommons.executeAction(gameBoard,2,2);
        visualizer.refresh(gameBoard);
        sleep(3000);

    }

    @Test
    public void testIsSimulationInFinalState() throws Exception {

    }

    @Test
    public void testCheckThreeInARow() throws Exception {

    }
}
