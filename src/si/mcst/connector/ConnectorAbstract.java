package si.mcst.connector;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 19:13
 */

public abstract class ConnectorAbstract {
    public abstract void executeAction(int actionFromParent);
}
