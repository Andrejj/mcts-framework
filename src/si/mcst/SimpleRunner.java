package si.mcst;

import si.mcst.executor.MCTSExecutor;
import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.executor.strategy.selection.impl.UCTSelection;
import si.mcst.executor.visualizer.VisualizerAbstract;
import si.mcst.executor.visualizer.impl.ConsoleVisualizer;
import si.tictactoe.impl.TicTacToeGameActions;
import si.tictactoe.impl.TicTacToeGameSimulator;
import si.mcst.executor.strategy.backpropagation.impl.AverageRewardBackPropagation;
import si.mcst.executor.strategy.expansion.imp.RandomExpansion;
import si.mcst.executor.strategy.selection.impl.RandomSelection;
import si.mcst.executor.strategy.simulation.impl.RandomSimulation;
import si.mcst.tree.MCTSearch;
import si.mcst.tree.node.NodeAbstract;
import si.mcst.tree.node.impl.SimpleNode;
import si.mcst.executor.visualizer.impl.SimpleGraphVisualizer;
import si.tictactoe.impl.TicTacToeVisualizer;

import javax.print.attribute.standard.DateTimeAtCompleted;
import java.io.IOException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 14:55
 */
public class SimpleRunner {
    public static void main(String [] args)
    {
        //Java7 issue fix (http://forum.jgraph.com/questions/3002/hierarchical-layout-throws-exception-with-large-graphs)
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

        int[][] gameBoard = new int[3][3];
        gameBoard = new int[][]{    {0, 0, 2},
                                    {0, 1, 1},
                                    {1, 0, 2}};
        int firstPlayer =1;
        int forPlayer=2;

        GameSimulatorAbstract gameConnector =  new TicTacToeGameSimulator(gameBoard,new TicTacToeGameActions(),forPlayer, firstPlayer);

        MCTSearch mctSearch = new MCTSearch(gameConnector);
        mctSearch.setRootNode(new SimpleNode());

        MCTSExecutor mctsExecutor = new MCTSExecutor(mctSearch);
        mctsExecutor.setSelectionStrategy(new UCTSelection());
        mctsExecutor.setExpansionStrategy(new RandomExpansion());
        mctsExecutor.setSimulationStrategy(new RandomSimulation());
        mctsExecutor.setBackPropagationStrategy(new AverageRewardBackPropagation());
        VisualizerAbstract visualizer= new SimpleGraphVisualizer(3);
        mctsExecutor.addGraphVisualizer(visualizer);
        mctsExecutor.addGraphVisualizer(new ConsoleVisualizer());
        //mctsExecutor.addGraphVisualizer(new TicTacToeVisualizer(gameBoard));

        NodeAbstract resultAction = mctsExecutor.ExecuteMCSTSimulation(10000,0);
        System.out.println("Choose action "+gameConnector.getActionName(resultAction.getActionFromParent()));
    }
}
