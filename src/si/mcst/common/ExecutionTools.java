package si.mcst.common;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 31.3.2013
 * Time: 17:05
 */
public class ExecutionTools {

    public static void DelayExecution(int delayPerSimulation) {
        try {
            Thread.sleep(delayPerSimulation);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
