package si.mcst.tree.node.impl;
import si.mcst.tree.node.NodeAbstract;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 14:38
 */
public class SimpleNode extends NodeAbstract{

    public SimpleNode(NodeAbstract parent, int actionFromParent) {
        super(parent,actionFromParent,!parent.isOpponentNode());
    }

    public SimpleNode(){
        super(null,0,true);
    }
}