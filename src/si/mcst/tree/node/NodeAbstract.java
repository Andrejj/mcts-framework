package si.mcst.tree.node;

import si.mcst.tree.MCTSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 14:36
 */
public abstract class NodeAbstract {
    protected NodeAbstract parent;
    protected HashMap<Integer,NodeAbstract> childs;
    protected int actionFromParent;

    protected boolean opponentNode;
    protected int visitCount = 0;
    protected double value = 0;

    public NodeAbstract(NodeAbstract parent, int actionFromParent){
        this(parent,actionFromParent,false);
    }

    public NodeAbstract(NodeAbstract parent, int actionFromParent,boolean opponentNode)
    {
        this.parent = parent;
        this.opponentNode = opponentNode;
        childs = new HashMap<Integer, NodeAbstract>();
        this.actionFromParent = actionFromParent;
    }

    public NodeAbstract getParent(){
        return this.parent;
    }

    public boolean isLeaf(){
        return (childs.size() == 0);
    }

    public boolean isRoot(){
        return (parent == null);
    }

    public List<NodeAbstract> getChilds(){
        return new ArrayList<NodeAbstract>(childs.values());
    }

    public HashMap<Integer,NodeAbstract> getActionsToChilds()
    {
        return childs;
    }

    public boolean isOpponentNode() {
        return opponentNode;
    }

    public NodeAbstract addChild(int action,NodeAbstract newNode)
    {
        childs.put(action,newNode);
        return newNode;
    }

    public int getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(int visitCount) {
        this.visitCount = visitCount;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getActionFromParent(){
        return actionFromParent;
    }
}
