package si.mcst.tree;

import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.tree.node.NodeAbstract;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 14:47
 */
public class MCTSearch {
    protected NodeAbstract root;
    GameSimulatorAbstract gameSimulator;

    public MCTSearch(GameSimulatorAbstract gameSimulator)
    {
        this.gameSimulator = gameSimulator;
    }

    public void setRootNode(NodeAbstract root) {
        this.root = root;
    }

    public NodeAbstract getRoot() {
        return root;
    }

    public GameSimulatorAbstract getGameSimulator(){
        return gameSimulator;
    }
}
