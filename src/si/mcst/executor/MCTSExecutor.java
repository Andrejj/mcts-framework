package si.mcst.executor;

import si.mcst.common.ExecutionTools;
import si.mcst.executor.strategy.backpropagation.BackPropagationAbstract;
import si.mcst.executor.strategy.expansion.ExpansionAbstract;
import si.mcst.executor.strategy.opponentmodel.OpponentModelAbstract;
import si.mcst.executor.strategy.selection.SelectionAbstract;
import si.mcst.executor.strategy.simulation.SimulationAbstract;
import si.mcst.executor.visualizer.VisualizerAbstract;
import si.mcst.tree.MCTSearch;
import si.mcst.tree.node.NodeAbstract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 14:57
 */
public class MCTSExecutor {
    protected MCTSearch executionTree;
    protected ExpansionAbstract expansionStrategy;
    protected SelectionAbstract selectionStrategy;
    protected BackPropagationAbstract backPropagationStrategy;
    protected SimulationAbstract simulationStrategy;
    protected int numberOfPlayouts = 1;

    public void setNumberOfPlayouts(int numberOfPlayouts) {
        this.numberOfPlayouts = numberOfPlayouts;
    }

    List<VisualizerAbstract> graphVisualizers;

    public MCTSExecutor(MCTSearch executionTree)
    {
        this.executionTree = executionTree;
        graphVisualizers = new ArrayList<VisualizerAbstract>();
    }

    public void setExpansionStrategy(ExpansionAbstract expansionStrategy) {
        this.expansionStrategy = expansionStrategy;
    }

    public void setSelectionStrategy(SelectionAbstract selectionStrategy) {
        this.selectionStrategy = selectionStrategy;
    }

    public void setBackPropagationStrategy(BackPropagationAbstract backPropagationStrategy) {
        this.backPropagationStrategy = backPropagationStrategy;
    }

    public void setSimulationStrategy(SimulationAbstract simulationStrategy) {
        this.simulationStrategy = simulationStrategy;
    }

    public void addGraphVisualizer(VisualizerAbstract visualizer) {
        visualizer.setTree(executionTree);
        graphVisualizers.add(visualizer);
    }

    private void refreshAllVisualizers(){
        for(VisualizerAbstract visualizer:graphVisualizers)
        {
            visualizer.refresh();
        }
    }

    public NodeAbstract ExecuteMCSTSimulation(int numOfIterations){
        return ExecuteMCSTSimulation(numOfIterations,0);
    }

    public NodeAbstract ExecuteMCSTSimulation(int numOfIterations,int delayPerSimulation)
    {
        if(1 > numOfIterations)
        {
            throw new IllegalArgumentException(String.format("Number of iteration is numOfIterations=%d but cannot be less then 1.",numOfIterations));
        }

        for (int i=0;i<numOfIterations;i++)
        {
            NodeAbstract currentNode = executionTree.getRoot();

            while(selectionStrategy.isFullyExpanded(currentNode,executionTree.getGameSimulator())){
                currentNode = selectionStrategy.selectChild(currentNode,executionTree.getGameSimulator());
            }

            if(!executionTree.getGameSimulator().isSimulationInFinalState())
            {
                currentNode = expansionStrategy.expand(currentNode,executionTree.getGameSimulator());
            }

            double gameResult = 0;
            for (int y=0;y<numberOfPlayouts;y++){
                gameResult += simulationStrategy.simulateGame(currentNode,executionTree.getGameSimulator());
            }

            while(currentNode !=null){
                backPropagationStrategy.backPropagate(currentNode,gameResult,numberOfPlayouts);
                currentNode = currentNode.getParent();
            }

            if(delayPerSimulation>0)
            {
                ExecutionTools.DelayExecution(delayPerSimulation);
            }
            refreshAllVisualizers();
            executionTree.getGameSimulator().resetSimulation();
        }

        return getBestChildGreedy(executionTree.getRoot());
    }

    private NodeAbstract getBestChildGreedy(NodeAbstract root) {
        List<NodeAbstract> childs = root.getChilds();
        double maxValue = Double.NEGATIVE_INFINITY;
        NodeAbstract selectedChild = null;
        for(NodeAbstract child:childs)
        {
            if(child.getValue()>maxValue)
            {
                maxValue=child.getValue();
                selectedChild = child;
            }
        }
        return selectedChild;
    }
}
