package si.mcst.executor.visualizer;

import si.mcst.tree.MCTSearch;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 19.3.2013
 * Time: 17:00
 */
public abstract class VisualizerAbstract {
    protected MCTSearch executionTree;

    protected VisualizerAbstract()
    {
    }

    public abstract void refresh();

    public void setTree(MCTSearch executionTree)
    {
        this.executionTree = executionTree;
    }
}
