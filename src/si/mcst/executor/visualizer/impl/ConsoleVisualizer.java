package si.mcst.executor.visualizer.impl;

import si.mcst.executor.visualizer.VisualizerAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 16:27
 */
public class ConsoleVisualizer extends VisualizerAbstract {
    private int counter = 0;

    public void refresh(int[][] playingBoard) {
        counter++;
        for(int x=0;x<3;x++)
        {
            for(int y=0;y<3;y++)
            {
                System.out.print("["+playingBoard[x][y]+"]");
            }
            System.out.println();
        }
    }

    @Override
    public void refresh() {
        counter++;
        System.out.println(counter);
    }
}
