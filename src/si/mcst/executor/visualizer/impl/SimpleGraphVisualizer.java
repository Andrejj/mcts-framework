package si.mcst.executor.visualizer.impl;

import si.mcst.executor.visualizer.VisualizerAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 19.3.2013
 * Time: 17:01
 */
public class SimpleGraphVisualizer extends VisualizerAbstract {
    private final SimpleGraphVisualizerFrame sgvf;


    public SimpleGraphVisualizer()
    {
        sgvf = new SimpleGraphVisualizerFrame();
    }

    public SimpleGraphVisualizer(int showDepth){
        this();
        sgvf.setShowDepth(showDepth);
    }

    @Override
    public void refresh() {
        sgvf.refresh(executionTree);
    }
}
