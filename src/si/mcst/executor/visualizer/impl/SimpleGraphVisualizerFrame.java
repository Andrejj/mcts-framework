package si.mcst.executor.visualizer.impl;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import si.mcst.executor.gamesimulator.GameActionsAbstract;
import si.mcst.tree.MCTSearch;
import si.mcst.tree.node.NodeAbstract;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 19.3.2013
 * Time: 17:02
 */
public class SimpleGraphVisualizerFrame extends JFrame{
    protected mxGraph graph;
    private static DecimalFormat df = new DecimalFormat("#.##");

    public void setShowDepth(int showDepth) {
        this.showDepth = showDepth;
    }

    private int showDepth = 0;

    public SimpleGraphVisualizerFrame()
    {
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(1024,860);
    }

    public void refresh(MCTSearch executionTree) {

        graph = new mxGraph();
        getContentPane().removeAll();

        Object graphRoot = graph.getDefaultParent();
        graph.getModel().beginUpdate();

        try
        {
            NodeAbstract rootNode = executionTree.getRoot();
            Object rootObject = graph.insertVertex(graphRoot, null, GetNodeDisplayName(rootNode), 20, 20, 50, 30);
            AddChildsNodes(graphRoot,rootObject,rootNode,0);

            mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
            layout.execute(graphRoot);
        }
        finally
        {
            graph.getModel().endUpdate();
        }

        mxGraphComponent graphComponent = new mxGraphComponent(graph);
        getContentPane().add(graphComponent);
        repaint();
        this.setVisible(true);
    }

    private String GetNodeDisplayName(NodeAbstract node) {
        String opponentNode = node.isOpponentNode() ? "*":"";
        String uct = "";
        if(node.getParent()!=null){
            uct = "!"+df.format(calculateUCT(node.getParent(),node));
        }
        return node.getVisitCount()+":"+df.format(node.getValue())+opponentNode+uct;
    }

    private void AddChildsNodes(Object graphRoot, Object rootObject, NodeAbstract rootNode,int currentDepth) {
        HashMap<Integer,NodeAbstract> actionsToChilds = rootNode.getActionsToChilds();
        if(showDepth>0 && currentDepth>=showDepth)
        {
            return;
        }
        currentDepth++;
        for(int action:actionsToChilds.keySet())
        {
            NodeAbstract childNode = actionsToChilds.get(action);
            Object childObject = graph.insertVertex(graphRoot,null,GetNodeDisplayName(childNode),20, 20, 50, 30);
//            graph.insertEdge(graphRoot,null,childNode.getTree().getGameSimulator().getActionName(action),rootObject,childObject);
            graph.insertEdge(graphRoot,null,action,rootObject,childObject);
            AddChildsNodes(graphRoot,childObject,childNode,currentDepth);
        }
    }

    private double calculateUCT(NodeAbstract parent, NodeAbstract child) {
        double result = 0;
        double c = 1/(2*Math.sqrt(2));
        result = child.getValue()+(2*c*Math.sqrt(2*Math.log((double)parent.getVisitCount())/(double)child.getVisitCount()));

        return result;
    }
}
