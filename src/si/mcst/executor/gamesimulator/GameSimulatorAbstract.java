package si.mcst.executor.gamesimulator;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 17:34
 */
public abstract class GameSimulatorAbstract {
    protected GameActionsAbstract gameActions;

    public abstract List<Integer> GetCurrentlyAvailableActions();
    public abstract void simulateAction(int selectedAction);
    public abstract void resetSimulation();
    public abstract boolean isSimulationInFinalState();

    public GameSimulatorAbstract(GameActionsAbstract gameActions)
    {
        this.gameActions = gameActions;
    }

    public String getActionName(int action)
    {
        return gameActions.getActionName(action);
    }

    public List<Integer> GetAllActions(){
        return gameActions.GetAllActions();
    }

    public abstract double getSimulationResult();
}
