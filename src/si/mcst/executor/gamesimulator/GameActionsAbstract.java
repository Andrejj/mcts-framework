package si.mcst.executor.gamesimulator;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 19.3.2013
 * Time: 18:20
 */
public abstract class GameActionsAbstract {
    public abstract String getActionName(int action);
    public abstract List<Integer> GetAllActions();
}
