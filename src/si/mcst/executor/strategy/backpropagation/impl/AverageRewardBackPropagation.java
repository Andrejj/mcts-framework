package si.mcst.executor.strategy.backpropagation.impl;

import si.mcst.executor.strategy.backpropagation.BackPropagationAbstract;
import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 18:46
 */
public class AverageRewardBackPropagation extends BackPropagationAbstract {
    @Override
    public void backPropagate(NodeAbstract currentNode, double gameResult, int numberOfPlayouts) {

        int newVisitCount = currentNode.getVisitCount()+numberOfPlayouts;
        double oldValue =  currentNode.getValue()*currentNode.getVisitCount();
        double newValue = (oldValue + (double)gameResult)/(double)newVisitCount;

        currentNode.setVisitCount(newVisitCount);
        currentNode.setValue(newValue);
    }
}
