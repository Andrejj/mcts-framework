package si.mcst.executor.strategy.backpropagation;

import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 18:44
 */
public abstract class BackPropagationAbstract {
    public abstract void backPropagate(NodeAbstract currentNode, double gameResult, int numberOfPlayouts);
}
