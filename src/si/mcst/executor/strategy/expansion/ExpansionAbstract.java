package si.mcst.executor.strategy.expansion;
import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.tree.node.NodeAbstract;

import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 16:52
 */
public abstract class ExpansionAbstract {

    protected int getNextAction(NodeAbstract node,GameSimulatorAbstract gameSimulator) {
        List<Integer> nonExploredActions = gameSimulator.GetCurrentlyAvailableActions();
        Set<Integer> keys = node.getActionsToChilds().keySet();
        nonExploredActions.removeAll(keys);
        int nextAction = SelectNextAction(nonExploredActions);
        return nextAction;
    }

    public NodeAbstract expand(NodeAbstract currentNode,GameSimulatorAbstract gameSimulator){
        int nextAction = getNextAction(currentNode,gameSimulator);
        NodeAbstract newNode = GetNewNode(currentNode, nextAction);
        currentNode.addChild(nextAction,newNode);
        gameSimulator.simulateAction(nextAction);
        return newNode;
    }

    protected abstract int SelectNextAction(List<Integer> actions);
    protected abstract NodeAbstract GetNewNode(NodeAbstract currentNode, int actionFromParent);

}
