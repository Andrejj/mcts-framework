package si.mcst.executor.strategy.expansion.imp;

import si.mcst.executor.strategy.expansion.ExpansionAbstract;
import si.mcst.tree.node.NodeAbstract;
import si.mcst.tree.node.impl.SimpleNode;

import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 16:55
 */
public class RandomExpansion extends ExpansionAbstract {
    Random random = new Random();

    public int SelectNextAction(List<Integer> nonExploredActions) {
        int numOfActions = nonExploredActions.size();
        int selectedActionNumber = random.nextInt(numOfActions);
        return nonExploredActions.get(selectedActionNumber);
    }

    @Override
    protected NodeAbstract GetNewNode(NodeAbstract currentNode, int actionFromParent) {
        SimpleNode newNode = new SimpleNode(currentNode,actionFromParent);

        return newNode;
    }
}
