package si.mcst.executor.strategy.opponentmodel;

import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 25.3.2013
 * Time: 17:52
 */
public abstract class OpponentModelAbstract {
    public abstract NodeAbstract selectChild(NodeAbstract currentNode);
}
