package si.mcst.executor.strategy.simulation.impl;

import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.executor.strategy.expansion.ExpansionAbstract;
import si.mcst.executor.strategy.expansion.imp.RandomExpansion;
import si.mcst.executor.strategy.simulation.SimulationAbstract;
import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 18:18
 */
public class RandomSimulation extends SimulationAbstract{
    ExpansionAbstract expansionStrategy = new RandomExpansion();

    @Override
    public double performeSimulation(NodeAbstract newNode,GameSimulatorAbstract gameSimulator) {
        double result = RecursiveSimulation(newNode,gameSimulator);
        return result;
    }

    private double RecursiveSimulation(NodeAbstract node,GameSimulatorAbstract gameSimulator) {
        if(gameSimulator.isSimulationInFinalState()){
            return gameSimulator.getSimulationResult();
        }

        NodeAbstract nextNode = expansionStrategy.expand(node,gameSimulator);
        return RecursiveSimulation(nextNode,gameSimulator);
    }
}
