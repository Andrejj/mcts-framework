package si.mcst.executor.strategy.simulation;

import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 18:17
 */
public abstract class SimulationAbstract {
    protected abstract double performeSimulation(NodeAbstract startNode,GameSimulatorAbstract gameSimulator);

    public double simulateGame(NodeAbstract startNode,GameSimulatorAbstract gameSimulator){
        double result = performeSimulation(startNode,gameSimulator);
        startNode.getActionsToChilds().clear();

        return result;
    }
}
