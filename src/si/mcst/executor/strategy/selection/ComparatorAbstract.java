package si.mcst.executor.strategy.selection;

import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 30.3.2013
 * Time: 15:33
 */
public abstract class ComparatorAbstract {
    public abstract boolean compare(double comparatingValue, double currentExtremeValue);

    public abstract double getNodeValue(double nodeValue, double currentNodeUct);
}
