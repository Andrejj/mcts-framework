package si.mcst.executor.strategy.selection.impl;

import si.mcst.executor.strategy.selection.ComparatorAbstract;
import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 30.3.2013
 * Time: 15:37
 */
public class MaximumComparator extends ComparatorAbstract {
    @Override
    public boolean compare(double comparatingValue, double currentExtremeValue) {
        if(comparatingValue>currentExtremeValue){
            return true;
        }
        return false;
    }

    @Override
    public double getNodeValue(double nodeValue, double currentNodeUct) {
        return nodeValue+currentNodeUct;
    }
}
