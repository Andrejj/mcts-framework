package si.mcst.executor.strategy.selection.impl;

import si.mcst.executor.strategy.selection.ComparatorAbstract;
import si.mcst.executor.strategy.selection.SelectionAbstract;
import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 22.3.2013
 * Time: 16:13
 */
public class UCTSelection extends RandomSelection {
    double c = 1/(2*Math.sqrt(2));
    int visitLimit = 0;
    ComparatorAbstract comparator=null;

    public UCTSelection(int visitLimit) {
        this();
        this.visitLimit = visitLimit;

    }

    public UCTSelection() {

    }

    @Override
    protected NodeAbstract SelectChild(NodeAbstract node) {
        double currentExtremeValue;
        NodeAbstract selectedNode = null;

        if (node.getVisitCount()<=visitLimit){
            return super.SelectChild(node);
        }

        boolean opponentNode = node.getChilds().get(0).isOpponentNode();

        if(!opponentNode){
            comparator = new MaximumComparator();
            currentExtremeValue = Double.NEGATIVE_INFINITY;
        }else{
            comparator = new MinimumComparator();
            currentExtremeValue = Double.POSITIVE_INFINITY;
        }

        for(NodeAbstract child:node.getChilds()){
            double currentNodeUct = calculateUCT(node,child,c);
            double currentNodeValue = comparator.getNodeValue(child.getValue(),currentNodeUct);
            if(comparator.compare(currentNodeValue,currentExtremeValue)){
                currentExtremeValue = currentNodeValue;
                selectedNode = child;
            }
        }

        return selectedNode;
    }

    private double calculateUCT(NodeAbstract parent, NodeAbstract child,double c) {
        double result = 0;
        result = 2*c*Math.sqrt(2*Math.log((double)parent.getVisitCount())/(double)child.getVisitCount());

        return result;
    }
}
