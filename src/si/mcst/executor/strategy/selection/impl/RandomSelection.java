package si.mcst.executor.strategy.selection.impl;

import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.executor.strategy.selection.SelectionAbstract;
import si.mcst.tree.node.NodeAbstract;

import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 15:31
 */
public class RandomSelection extends SelectionAbstract {
    static Random random = new Random();

    public RandomSelection()
    {

    }

    @Override
    protected NodeAbstract SelectChild(NodeAbstract node) {
        List<NodeAbstract> childs = node.getChilds();
        int numOfChilds = childs.size();
        int selectedChildNumber = random.nextInt(numOfChilds);
        return childs.get(selectedChildNumber);
    }

    @Override
    public boolean isFullyExpanded(NodeAbstract currentNode, GameSimulatorAbstract gameSimulator) {

        //If leaf, expand
        if(currentNode.isLeaf())
        {
            return false;
        }

        //If not all actions added, expand
        if(currentNode.getActionsToChilds().keySet().size() < gameSimulator.GetCurrentlyAvailableActions().size())
        {
            return false;
        }

        //If all actions exist, select
        return true;
    }
}
