package si.mcst.executor.strategy.selection;

import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.mcst.executor.strategy.simulation.SimulationAbstract;
import si.mcst.tree.MCTSearch;
import si.mcst.tree.node.NodeAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 15:32
 */
public abstract class SelectionAbstract{

    public NodeAbstract selectChild(NodeAbstract rootNode, GameSimulatorAbstract gameSimulator){
        NodeAbstract selectedChild = null;

        selectedChild =  SelectChild(rootNode);

        int actionFromParent= selectedChild.getActionFromParent();

        gameSimulator.simulateAction(actionFromParent);

        return selectedChild;
    }

    protected abstract NodeAbstract SelectChild(NodeAbstract node);

    public abstract boolean isFullyExpanded(NodeAbstract currentNode, GameSimulatorAbstract gameSimulator);
}
