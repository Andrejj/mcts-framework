package si.tictactoe;

import si.mcst.common.ExecutionTools;
import si.mcst.executor.strategy.selection.impl.UCTSelection;
import si.mcst.executor.visualizer.impl.ConsoleVisualizer;
import si.mcst.executor.visualizer.impl.SimpleGraphVisualizer;
import si.tictactoe.impl.TicTacToeConnector;
import si.mcst.executor.MCTSExecutor;
import si.mcst.executor.gamesimulator.GameSimulatorAbstract;
import si.tictactoe.impl.TicTacToeGameActions;
import si.tictactoe.impl.TicTacToeGameSimulator;
import si.mcst.executor.strategy.backpropagation.impl.AverageRewardBackPropagation;
import si.mcst.executor.strategy.expansion.imp.RandomExpansion;
import si.mcst.executor.strategy.selection.impl.RandomSelection;
import si.mcst.executor.strategy.simulation.impl.RandomSimulation;
import si.mcst.tree.MCTSearch;
import si.mcst.tree.node.NodeAbstract;
import si.mcst.tree.node.impl.SimpleNode;
import si.tictactoe.impl.TicTacToeVisualizer;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 19:04
 */
public class TicTacToeRunner {
    static SimpleGraphVisualizer graphVisualizer = new SimpleGraphVisualizer(1);

    public static void main(String [] args)
    {
        //Java7 issue fix (http://forum.jgraph.com/questions/3002/hierarchical-layout-throws-exception-with-large-graphs)
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        TicTacToeVisualizer gameVisualizer = new TicTacToeVisualizer();
        ConsoleVisualizer consoleVisualizer = new ConsoleVisualizer();

        int totalActions=0;
        int drawActions = 0;

        while(true)
        {
            int numberOfSimulations = 5000;
            int delayBetweenIterations = 0;

            int[][] gameBoard = new int[][]{    {0, 0, 0},
                                                {0, 0, 0},
                                                {0, 0, 0}};
            int iteration = 0;
            int firstPlayer = 1;

            TicTacToeConnector realGame = new TicTacToeConnector(gameBoard);
            while(!realGame.isGameOver())
            {
                int nextPlayer = (iteration%2)+1;
                int nextAction =  GetNextAction(gameBoard, numberOfSimulations,nextPlayer,firstPlayer);
                realGame.executeAction(nextAction,(iteration%2)+1);
                iteration++;
//                gameVisualizer.refresh(gameBoard);

                if(delayBetweenIterations>0)
                {
                    ExecutionTools.DelayExecution(delayBetweenIterations);
                }
            }

            if(realGame.getWinner() == 0){
                drawActions++;
            }
            totalActions++;

            System.out.println("Game winner:"+realGame.getWinner() +" Draw rate "+drawActions+"/"+totalActions+"("+(double)drawActions/(double)totalActions+")");
        }
    }

    private static int GetNextAction(int[][] gameBoard,int numberOfSimulations,int forPlayer,int firstPlayer)
    {
        MCTSExecutor executor = GetTicTacToeExecutor(gameBoard,forPlayer,firstPlayer);
        NodeAbstract nextNode = executor.ExecuteMCSTSimulation(numberOfSimulations,0);
//        graphVisualizer.setTree(nextNode.getTree());
        return nextNode.getActionFromParent();
    }

    private static MCTSExecutor GetTicTacToeExecutor(int[][] gameBoard,int forPlayer,int firstPlayer)
    {
        GameSimulatorAbstract gameSimulator =  new TicTacToeGameSimulator(gameBoard,new TicTacToeGameActions(),forPlayer, firstPlayer);

        MCTSearch mctSearch = new MCTSearch(gameSimulator);
        mctSearch.setRootNode(new SimpleNode());

        MCTSExecutor mctsExecutor = new MCTSExecutor(mctSearch);
        mctsExecutor.setSelectionStrategy(new UCTSelection());
        mctsExecutor.setExpansionStrategy(new RandomExpansion());
        mctsExecutor.setSimulationStrategy(new RandomSimulation());
        mctsExecutor.setBackPropagationStrategy(new AverageRewardBackPropagation());
//        mctsExecutor.addGraphVisualizer(new SimpleGraphVisualizer(1));

        return mctsExecutor;
    }
}
