package si.tictactoe.impl;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 19:24
 */
public class TicTacToeCommons {
    public static ArrayList<Integer> GetCurrentlyAvailableActions(int[][] playingBoard) {
        ArrayList<Integer> availableActions = new ArrayList<Integer>(9);
        for (int x=0;x<3;x++)
        {
            for (int y=0;y<3;y++)
            {
                if(playingBoard[x][y] == 0)
                {
                    availableActions.add(x+(y*3));
                }
            }
        }
        return availableActions;
    }

    public static void executeAction(int[][] playingBoard, int selectedAction, int player) {
        if(playingBoard[selectedAction%3][selectedAction/3] != 0)
        {
            throw new RuntimeException("Already used ");
        }
        playingBoard[selectedAction%3][selectedAction/3] = player;
    }

    public static boolean isSimulationInFinalState(int[][] playingBoard) {
        int winner = checkThreeInARow(playingBoard);
        return (winner>0)||(GetCurrentlyAvailableActions(playingBoard).size() == 0);
    }

    public static int checkThreeInARow(int[][] playingBoard) {
        for(int x=0;x<3;x++)
        {
            //Check columns
            if ((playingBoard[x][0] == playingBoard[x][1]) && (playingBoard[x][0] == playingBoard[x][2])){
                if(playingBoard[x][0]>0)
                {
                    return playingBoard[x][0];
                }
            }

            //Check rows
            if ((playingBoard[0][x] == playingBoard[1][x]) && (playingBoard[0][x] == playingBoard[2][x])){
                if(playingBoard[0][x]>0)
                {
                    return playingBoard[0][x];
                }
            }
        }
        //Check diagonals
        if(((playingBoard[0][0] == playingBoard[1][1]) && (playingBoard[0][0] == playingBoard[2][2]))||
                ((playingBoard[2][0] == playingBoard[1][1]) && (playingBoard[2][0] == playingBoard[0][2]))){
            if(playingBoard[1][1]>0)
            {
                return playingBoard[1][1];
            }
        }
        return 0;
    }
}
