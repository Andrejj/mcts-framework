package si.tictactoe.impl;

import si.mcst.executor.gamesimulator.GameActionsAbstract;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 18:02
 */
public class TicTacToeGameActions extends GameActionsAbstract {
    private enum GameActions{
        A1,A2,A3,B1,B2,B3,C1,C2,C3
    }

    @Override
    public String getActionName(int action) {
        return GameActions.values()[action].name();
    }

    @Override
    public List<Integer> GetAllActions() {
        return Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
    }



}
