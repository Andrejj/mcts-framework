package si.tictactoe.impl;

import si.mcst.executor.gamesimulator.GameActionsAbstract;
import si.mcst.executor.gamesimulator.GameSimulatorAbstract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 15.3.2013
 * Time: 17:41
 */
public class TicTacToeGameSimulator extends GameSimulatorAbstract {
    public int[][] playingBoard;
    protected int[][] simulatingBoard;
    private final int forPlayer;
    private final int firstPlayer;

    public TicTacToeGameSimulator(int[][] playingBoard, GameActionsAbstract gameActions, int forPlayer, int firstPlayer)
    {
        super(gameActions);
        this.playingBoard = playingBoard;
        this.firstPlayer = firstPlayer;
        resetSimulation();
        this.forPlayer = forPlayer;
    }

    @Override
    public List<Integer> GetCurrentlyAvailableActions() {
        ArrayList<Integer> availableActions = TicTacToeCommons.GetCurrentlyAvailableActions(simulatingBoard);
        return availableActions;
    }



    @Override
    public void simulateAction(int selectedAction) {
        int nextPlayer = getNextPlayer();
        TicTacToeCommons.executeAction(simulatingBoard,selectedAction,nextPlayer);
    }

    private int getNextPlayer() {
        int count1 = 0;
        int count2 = 0;

        for(int x=0;x<3;x++)
        {
            for(int y=0;y<3;y++)
            {
                if(simulatingBoard[x][y]==1)
                {
                    count1++;
                }
                if(simulatingBoard[x][y]==2)
                {
                    count2++;
                }
            }
        }
        if(count1>count2){
            return 2;
        } else if(count2>count1)
        {
            return 1;
        }
        return firstPlayer;
    }


    @Override
    public void resetSimulation() {

        simulatingBoard = new int[3][3];
        for(int x=0;x<3;x++)
        {
            for(int y=0;y<3;y++)
            {
                 simulatingBoard[x][y] = playingBoard[x][y];
            }
        }
    }

    @Override
    public boolean isSimulationInFinalState() {
        return TicTacToeCommons.isSimulationInFinalState(simulatingBoard);
    }



    @Override
    public double getSimulationResult() {
        int winner = TicTacToeCommons.checkThreeInARow(simulatingBoard);

        if(winner ==0){
            return 0.5;
        } else if (winner ==1 && forPlayer == 1)
        {
            return 1;
        }else if(winner == 2 && forPlayer == 2){
            return 1;
        }
        return 0;

    }
}
