package si.tictactoe.impl;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 18:50
 */
public class TicTacToeVisualizerVisualizerFrame extends JFrame{
    int[][] gameBoard;

    public void refresh(int[][] gameBoard) {
        this.gameBoard = gameBoard;
        getContentPane().removeAll();
        setSize(400,400);
        repaint();
        this.setVisible(true);
    }

    public void paint(Graphics g)  {
        super.paint(g);
        g.drawLine(150,50,150,350);
        g.drawLine(250,50,250,350);
        g.drawLine(50,150,350,150);
        g.drawLine(50,250,350,250);

        for(int x=0;x<3;x++)
        {
            for(int y=0;y<3;y++)
            {
                if(gameBoard[x][y] == 1)
                {
                    drawCircle(g, x, y);
                }else if(gameBoard[x][y]==2)
                {
                    drawCross(g,x,y);
                }

            }
        }
    }

    private void drawCross(Graphics g, int y, int x) {
        g.setColor(Color.RED);
        int xB = 55+(x*100);
        int yB = 55+(y*100);
        g.drawLine(xB,yB,xB+90,yB+90);
        g.drawLine(xB+90,yB,xB,yB+90);
    }

    private void drawCircle(Graphics g, int y, int x) {
        g.setColor(Color.BLUE);
        g.fillOval(55+(x*100),55+(y*100),90,90);
    }
}
