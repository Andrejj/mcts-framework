package si.tictactoe.impl;

import si.mcst.executor.visualizer.VisualizerAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 18:50
 */
public class TicTacToeVisualizer{
    private final TicTacToeVisualizerVisualizerFrame tttv;

    public TicTacToeVisualizer()
    {
        tttv = new TicTacToeVisualizerVisualizerFrame();
    }
    public void refresh(int[][] gameBoard) {

        tttv.refresh(gameBoard);
    }
}
