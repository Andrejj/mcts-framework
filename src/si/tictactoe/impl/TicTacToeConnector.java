package si.tictactoe.impl;

import si.mcst.connector.ConnectorAbstract;

/**
 * Created with IntelliJ IDEA.
 * User: AndrejBabic
 * Date: 21.3.2013
 * Time: 19:14
 */
public class TicTacToeConnector extends ConnectorAbstract {
    private int[][] gameBoard;

    public TicTacToeConnector(int[][] gameBoard) {
        this.gameBoard = gameBoard;
    }

    public boolean isGameOver() {
        return TicTacToeCommons.isSimulationInFinalState(gameBoard);
    }

    public int getWinner(){
        return TicTacToeCommons.checkThreeInARow(gameBoard);
    }
    
    @Override
    public void executeAction(int actionFromParent) {

    }

    public void executeAction(int actionFromParent, int player) {
        TicTacToeCommons.executeAction(gameBoard,actionFromParent,player);
    }
}
